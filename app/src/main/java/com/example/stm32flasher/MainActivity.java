package com.example.stm32flasher;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {


    String address = null;
    private ProgressDialog progress;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    private boolean isBtConnected = false;

    Button sendButton;
    EditText sendlText;
    TextView serialText;

    //SPP UUID. Look for it
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private static final String TAG = "MY_APP_DEBUG_TAG";

    final int MESSAGE_READ = 0;
    final int MESSAGE_WRITE = 1;
    final int MESSAGE_TOAST = 2;

    final char GET_CMDS    = 0x00;
    final char GET_BL_VR   = 0x01;
    final char GET_ID      = 0x02;
    final char GO_TO       = 0x21;
    final char WRITE_MEM   = 0x31;
    final char ERASE       = 0x43;
    final char E_ERASE     = 0x44;
    final char W_PROTECT   = 0x63;
    final char W_UNPROTECT = 0x73;
    final char R_PROTECT   = 0x82;
    final char R_UNPROTECT = 0x92;


    ConnectedThread btThread;

    Handler handler =  new Handler(){
        @Override
        public void handleMessage(Message msg) {

            if(msg.what == MESSAGE_READ){

                serialText.append((String)msg.obj);
            }
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sendButton = findViewById(R.id.sendButton);
        sendlText = findViewById(R.id.sendText);
        serialText = findViewById(R.id.serialText);

        Intent newint = getIntent();
        address = newint.getStringExtra(BtConnect.EXTRA_ADDRESS); //receive the address of the bluetooth device

        serialText.setMovementMethod(ScrollingMovementMethod.getInstance());
        serialText.setSelected(true);
        serialText.setText("");

        new ConnectBT().execute(); //Call the class to connect

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                String hex = sendlText.getText().toString();

                if(hex.length()%2 == 0) {


                    int index = 0;
                    byte[] data = new byte[hex.length() / 2];

                    for (int i = 0; i < hex.length(); i += 2) {
                        String str = hex.substring(i, i + 2);
                        data[index++] = ((byte) Integer.parseInt(str, 16));
                    }

                    serialText.append(new String(data));
                    serialText.append("\n");

                    btThread.write(data);
                }
            }
        });

    }


    private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute()
        {
            progress = ProgressDialog.show(MainActivity.this, "Connecting...", "Please wait!!!");  //show a progress dialog
        }

        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            try
            {
                if (btSocket == null || !isBtConnected)
                {
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice BT = myBluetooth.getRemoteDevice(address);//connects to the device's address and checks if it's available
                    btSocket = BT.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();//start connection
                }
            }
            catch (IOException e)
            {
                ConnectSuccess = false;//if the try failed, you can check the exception here
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess)
            {
                msg("Connection Failed. Is it a SPP Bluetooth? Try again.");
                finish();
            }
            else
            {
                msg("Connected.");
                isBtConnected = true;

                btThread = new ConnectedThread(btSocket);
                btThread.start();
            }
            progress.dismiss();
        }
    }

    // fast way to call Toast
    private void msg(String s)
    {
        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
    }


    private class ConnectedThread extends Thread {


        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private byte[] mmBuffer; // mmBuffer store for the stream

        public ConnectedThread(BluetoothSocket socket) {


            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams; using temp objects because
            // member streams are final.
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating input stream", e);
            }
            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating output stream", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {

            mmBuffer = new byte[128];
            int numBytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs.
            while (true) {
                try {

                    // Read from the InputStream.
                    numBytes = mmInStream.read(mmBuffer);
                    // Send the obtained bytes to the UI activity.

                    if (numBytes != -1) {

                        String text = new String(mmBuffer, 0, numBytes);

                        if (text.contains("\n")) {
                            Message readMsg = new Message();

                            readMsg.what = MESSAGE_READ;
                            readMsg.setTarget(handler);
                            readMsg.arg1 = numBytes;
                            readMsg.obj = text;
                            readMsg.sendToTarget();
                        }
                    }


                } catch (IOException e) {
                    Log.d(TAG, "Input stream was disconnected", e);
                    break;
                }
            }
        }

        // Call this from the main activity to send data to the remote device.
        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);

                // Share the sent message with the UI activity.
                Message writtenMsg = handler.obtainMessage(
                        MESSAGE_WRITE, -1, -1, mmBuffer);
                writtenMsg.sendToTarget();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when sending data", e);

                // Send a failure message back to the activity.
                Message writeErrorMsg =
                        handler.obtainMessage(MESSAGE_TOAST);
                Bundle bundle = new Bundle();
                bundle.putString("toast",
                        "Couldn't send data to the other device");
                writeErrorMsg.setData(bundle);
                handler.sendMessage(writeErrorMsg);
            }
        }

        // Call this method from the main activity to shut down the connection.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the connect socket", e);
            }
        }
    }

}

































